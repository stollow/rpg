package fr.zenika.RapidPeonGenerator.service;

import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HeroesServices {

    public HeroRepository repository;

    @Autowired
    public HeroesServices (HeroRepository repo){
        this.repository = repo;
    }

    public List<Hero> getAllHeroes(){
        return (List<Hero>)this.repository.findAll();
    }
}
