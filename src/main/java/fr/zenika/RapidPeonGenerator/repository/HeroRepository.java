package fr.zenika.RapidPeonGenerator.repository;

import fr.zenika.RapidPeonGenerator.models.Hero;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface HeroRepository extends CrudRepository<Hero,String> {
}
