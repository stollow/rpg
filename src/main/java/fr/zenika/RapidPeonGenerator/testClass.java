package fr.zenika.RapidPeonGenerator;

import fr.zenika.RapidPeonGenerator.factories.HeroesFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class testClass implements CommandLineRunner {

    private HeroesFactory factory;

    @Autowired
    public testClass(HeroesFactory factory){
        this.factory = factory;
    }

    @Override
    public void run(String... args) throws Exception {
        factory.createHeroe("jeanne","Human",50,'f');
        factory.createHeroe("Thrall","Orc",20,'m');
    }
}
