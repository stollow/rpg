package fr.zenika.RapidPeonGenerator.models;

import fr.zenika.RapidPeonGenerator.enums.StatsEnum;

import javax.persistence.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Entity
public class Hero {

    @Id
    private String id;
    private String name;
    private String race;
    private int gold;
    private char sexe;
    @ElementCollection
    @CollectionTable(name = "stats_mapping",
            joinColumns = {@JoinColumn(name = "stat_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "name")
    @Column(name = "stat")
    Map<String,Integer> stats;

    public Hero(String id, String name, String race, int gold, char sexe, int maxStat, Map<String,Integer> stats) {
        this.id = id;
        this.name = name;
        this.race = race;
        this.gold = gold;
        this.stats = stats;
        this.sexe = sexe;
        this.setStats(maxStat);
    }

    public Hero(){

    }

    public char getSexe() {
        return sexe;
    }

    public Map<String, Integer> getStats() {
        return stats;
    }

    public void setStats(int max){
        int i = 0;
        int result = 0;

        List<String> list = new ArrayList<>(stats.keySet());
        Collections.shuffle(list);

        Map<String, Integer> shuffleMap = new LinkedHashMap<>();
        list.forEach(k->shuffleMap.put(k, stats.get(k)));
        for (Map.Entry<String, Integer> entry : shuffleMap.entrySet()) {
            i++;
            String k = entry.getKey();
            Integer v = entry.getValue();
            if(i<stats.size()) {
                 result = ThreadLocalRandom.current().nextInt(max / stats.size()-i - 20/i, max / stats.size()-i + 20*i);
            } else {
                result = max;
            }
            stats.put(k, result);
            max -= result;
        }

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

}
