package fr.zenika.RapidPeonGenerator.factories;

import fr.zenika.RapidPeonGenerator.enums.StatsEnum;
import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class HeroesFactory {

    public HeroRepository repository;


    @Autowired
    public HeroesFactory (HeroRepository repo){
        this.repository = repo;
    }

    public HeroesFactory(){}



    public Hero createHeroe(String name, String race, int gold,char sexe){
        Map<String,Integer> stats = new HashMap<>();
        for(StatsEnum stat : StatsEnum.values()){
            stats.put(stat.getName(),0);
        }
        Hero hero = new Hero(UUID.randomUUID().toString(),name,race,gold,sexe,300,stats);
        repository.save(hero);
        return hero;
    }

}
