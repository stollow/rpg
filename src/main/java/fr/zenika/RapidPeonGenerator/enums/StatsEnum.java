package fr.zenika.RapidPeonGenerator.enums;

public enum StatsEnum {
    PV("Vigor"),
    DEF("Agility"),
    STRENGTH("Strength"),
    LUCK("Luck"),
    INTELLIGENCE("Intelligence");

    private String name;

    StatsEnum (String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
