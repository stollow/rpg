package fr.zenika.RapidPeonGenerator;

import fr.zenika.RapidPeonGenerator.factories.HeroesFactory;
import fr.zenika.RapidPeonGenerator.repository.HeroRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RapidPeonGeneratorApplication{

	public static void main(String[] args) {
		SpringApplication.run(RapidPeonGeneratorApplication.class, args);
	}

}
