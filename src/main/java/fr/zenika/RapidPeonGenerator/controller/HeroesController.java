package fr.zenika.RapidPeonGenerator.controller;

import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.pojo.HeroRepresentation;
import fr.zenika.RapidPeonGenerator.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/heroes")
public class HeroesController {

    public HeroRepository repository;
    public HeroRepresentation representation;

    @Autowired
    public HeroesController (HeroRepository repo){
        this.repository = repo;
    }

    @GetMapping
    public List<HeroRepresentation> getAll() {
        List<Hero> heroes =(List<Hero>) this.repository.findAll();
        return heroes.stream()
                .map(h -> new HeroRepresentation(h.getId(),h.getName(),h.getRace(),h.getGold(),h.getSexe(),h.getStats()))
                .collect(Collectors.toList());
    }

}
