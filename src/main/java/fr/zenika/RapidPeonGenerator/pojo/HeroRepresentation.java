package fr.zenika.RapidPeonGenerator.pojo;

import javax.persistence.*;
import java.util.Map;

public class HeroRepresentation {

    private String id;
    private String name;
    private char sexe;
    private String race;
    private int gold;
    Map<String,Integer> stats;

    public HeroRepresentation(String id, String name, String race, int gold, char sexe, Map<String,Integer> stats) {
        this.id = id;
        this.name = name;
        this.race = race;
        this.gold = gold;
        this.sexe = sexe;
        this.stats = stats;
    }

    public String getId() {
        return id;
    }

    public Map<String, Integer> getStats() {
        return stats;
    }

    public void setStats(Map<String, Integer> stats) {
        this.stats = stats;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
